package com.linwl.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.linwl.consumer.domain.User;
import com.linwl.consumer.feign.UserFeignClient;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("user")
public class UserController {
	/*@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private EurekaClient eurekaClient;*/
	@Autowired
	private UserFeignClient userFeignClient;
	
	@GetMapping("find/{id}")
	@HystrixCommand(fallbackMethod = "findByIdFallBack")
	public User findById(@PathVariable Long id) {
		//User user = restTemplate.getForObject("http://localhost:8081/find/1",User.class);
		//InstanceInfo instance = eurekaClient.getNextServerFromEureka("PROVIDER", false);
		//User user  =restTemplate.getForObject(instance.getHomePageUrl()+"/find/"+id, User.class);
		//return user;
		return userFeignClient.findById(id);
	}
	public User findByIdFallBack(Long id) {
		User user = new User();
		user.setId(0L);
		user.setName("熔断器");
		user.setAge(110);
		return user;
	}
	
	
	
	
	@PostMapping("save")
	public User save(User user) {
		return userFeignClient.save(user);
	}
}
