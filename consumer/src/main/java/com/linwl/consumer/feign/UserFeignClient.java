package com.linwl.consumer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.linwl.consumer.domain.User;

@FeignClient("PROVIDER")
public interface UserFeignClient {
	
	//RESTFUL 请求方式
	//@RequestMapping(value="user/find/{id}",method=RequestMethod.GET)
	@GetMapping("user/find/{id}")
	public User findById(@PathVariable("id") Long id);
	//消费端
	/**
	 * 
	 */
	@RequestMapping(value="user/save",method=RequestMethod.POST)
	public User save(@RequestBody User user);
}
